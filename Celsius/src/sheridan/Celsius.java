package sheridan;

public class Celsius {

	public static void main(String[] args) {
		System.out.println("Temperature in Celsius: " + Celsius.convertFromFahrenheit(32));

	}
	
	public static int convertFromFahrenheit(int temperature) {
		int celsius =(( 5 * (temperature - 32)) / 9);  
		return Math.round(celsius);
	}

}

