package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testconvertFromFahrenheit() {
		int celsius = Celsius.convertFromFahrenheit(32);
		assertTrue("The temperature was not calculated properly", celsius == 0);
	}
	
	@Test
	public void testconvertFromFahrenheitNegative() {
		int celsius = Celsius.convertFromFahrenheit(-5);
		assertFalse("The temperature was not calculated properly" , celsius == -21);
	}
	
	@Test
	public void testconvertFromFahrenheitBoundryIn() {
		int celsius = Celsius.convertFromFahrenheit(6);
		assertTrue("The temperature was not calculated properly", celsius == -14);
	}
	
	@Test
	public void testconvertFromFahrenheitBoundryOut() {
		int celsius = Celsius.convertFromFahrenheit(7);
		assertFalse("The temperature was not calculated properly" , celsius == -14);
	}
	
	

}
